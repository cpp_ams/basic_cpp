/*
 * Swarm.h
 *
 *  Created on: Aug 24, 2018
 *      Author: ansc
 */

#ifndef SWARM_H_
#define SWARM_H_

#include "Particle.h"

namespace ams {

	class Swarm {
	public:
		const static int NPARTICLES = 5000;

	private:
		Particle * m_pParticles;
		int lastTime;

	public:
		Swarm();
		virtual ~Swarm();

		void update(int elapsed);
		const Particle * const getParticles() { return m_pParticles; };
	};

} /* namespace ams */

#endif /* SWARM_H_ */
