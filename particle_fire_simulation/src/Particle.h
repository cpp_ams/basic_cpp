/*
 * Particle.h
 *
 *  Created on: Aug 24, 2018
 *      Author: ansc
 */

#ifndef PARTICLE_H_
#define PARTICLE_H_

#include <stdlib.h>

namespace ams {

	struct Particle {
		double m_x;
		double m_y;

	private:
		double m_direction;
		double m_speed;

	private:
		void init();

	public:
		Particle();
		virtual ~Particle();

		void update(int interval);
	};

} /* namespace ams */

#endif /* PARTICLE_H_ */
