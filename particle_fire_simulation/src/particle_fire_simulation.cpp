#include <iostream>
#include <SDL.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

#include "Screen.h"
#include "Swarm.h"

using namespace std;
using namespace ams;

int main(int argc, char* argv[]) {

	srand(time(NULL));

	Screen screen;

	if (screen.init() == false) {
		cout << "Error initialising SDL." << endl;
	}

	Swarm swarm;

	int halfWidth = Screen::SCREEN_WIDTH / 2;

	while (true) {
		// update particles
		// draw particles

		int elapsed = SDL_GetTicks();

		swarm.update(elapsed);

		unsigned char red = static_cast<unsigned char>((1 + sin(elapsed * 0.0006)) * 128); // unsigned char to prevent values over 255
		unsigned char green = static_cast<unsigned char>((1 + sin(elapsed * 0.0002)) * 128); // unsigned char to prevent values over 255
		unsigned char blue = static_cast<unsigned char>((1 + sin(elapsed * 0.0003)) * 128); // unsigned char to prevent values over 255

		const Particle * const pParticles = swarm.getParticles();

		for ( int i = 0; i < Swarm::NPARTICLES; i++) {
			Particle particle = pParticles[i];

			int x = (particle.m_x + 1) * halfWidth;
			int y = particle.m_y * halfWidth + Screen::SCREEN_HEIGHT / 2;

			screen.setPixel(x, y, red, green, blue);
		}

		screen.boxBlur();

		// draw the screen
		screen.update();
		// check for messages/events
		if (screen.processEvents() == false) {
			break;
		}
	}

	screen.close();

	return 0;
}
