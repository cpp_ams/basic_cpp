################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Particle.cpp \
../src/Screen.cpp \
../src/Swarm.cpp \
../src/particle_fire_simulation.cpp 

OBJS += \
./src/Particle.o \
./src/Screen.o \
./src/Swarm.o \
./src/particle_fire_simulation.o 

CPP_DEPS += \
./src/Particle.d \
./src/Screen.d \
./src/Swarm.d \
./src/particle_fire_simulation.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -I/usr/include/SDL2 -O3 -Wall -c -fmessage-length=0  -std=c++11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


